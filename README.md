# Presentation to the MemTech Guild Meeting

Presented on 09/04/2018, covering the topic: Introduction to R and RStudio

### Presentation Outline

- Things to Know About R
- Features of R as a Programming Language
- Vectors and More Complex Data Structures
- Introducing the Tidyverse
- Tidy Data
- Manipulating Data with dplyr
- Visualizing Data with ggplot2
- Other Notable Tools and Packages
- Interactive Plots with shiny
- Downloading and Installing R and RStudio

### Repository Contents

This repository contains the presentation given (presentation.html), a sample notebook (pres_notebook.html), the source code for the shiny app demonstrated (/demo), and the source code for the presentation and sample notebook.